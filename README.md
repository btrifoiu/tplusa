# Assumptions
terraform and ansible are already installed and available in path

# Configuration
There are a bunch of configuration items that can be set in _ansible/vars_.

## Environment
To create a new environment one must create a new directory in vars

```bash
mkdir ansible/vars/new_env
```

Each environment must have two environment configuration files under _ansible/vars/new_env_:
1. _ansible/vars/new_env/new_env.yaml_ that holds naming schema
2. _ansible/vars/new_env/new_env0secrets.yaml_ that holds AWS credentials

## Cloud
In each environment there can be multiple clouds or regions.
Each cloud must have a cloud config file under _ansible/vars/environmnet/cloud.yaml_.

See the example files.

# Run command
```bash
cd ansible
ansible-playbook -e environment_id=new_env -e cloud_id=cloud terraformation.yaml
```
