provider "aws" {
  region = "${var.region}"
}

resource "aws_instance" "public_instance" {
  count = "${var.number_of_instances}"

  instance_type               = "${var.instance_type}"
  ebs_optimized               = "${var.ebs_optimized}"
  ami                         = "${var.aws_ami}"
  key_name                    = "${var.instance_key_name}"
  vpc_security_group_ids      = ["${var.secgrp_id}"]
  subnet_id                   = "${element(split(",", var.subnet_id), count.index)}"
  associate_public_ip_address = true
  source_dest_check           = true
  disable_api_termination     = true

  tags = {
    // Takes the instance_short_name input variable and adds
    //  the count.index to the name., e.g.
    //  "example-host-web-1"
    Name = "${var.instance_short_name}-${count.index}"
  }

  root_block_device {
    volume_type           = "${var.voltype}"
    volume_size           = "${var.volsize}"
    delete_on_termination = true
  }

  provisioner "remote-exec" {
    inline = ["sudo apt-get -qq install python -y"]

    connection {
      type        = "ssh"
      host        = "${self.public_ip}"
      user        = "${var.instance_ssh_user}"
      private_key = "${file("${var.ssh_private_key_path}")}"
    }
  }

  provisioner "local-exec" {
    command = "export ANSIBLE_HOST_KEY_CHECKING=False && ansible-playbook -i ${self.public_ip}, -u ${var.instance_ssh_user} --private-key ${var.ssh_private_key_path} ${var.playbook_path}"
  }
}
