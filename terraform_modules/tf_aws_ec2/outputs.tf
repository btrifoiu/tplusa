// Output the ID of the EC2 instance created
output "ec2_instance_public_subnet_private_ip" {
  value = "${split(",",join(",", aws_instance.public_instance.*.private_ip))}"
}

output "ec2_instance_public_subnet_public_ip" {
  value = "${join(",", aws_instance.public_instance.*.public_ip)}"
}

output "ec2_public_instance_id" {
  value = "${split(",",join(",", aws_instance.public_instance.*.id))}"
}
