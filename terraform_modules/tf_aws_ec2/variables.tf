// Module specific variables

variable "instance_domain" {
  description = "Used to populate the Name tag. This is done in main.tf"
}

variable "instance_ssh_user" {
  default = "ubuntu"
}

variable "instance_short_name" {}

variable "instance_key_name" {}

variable "ssh_private_key_path" {}

variable "instance_type" {}

variable "voltype" {}

variable "volsize" {}

variable "region" {}

variable "playbook_path" {}

variable "subnet_id" {
  description = "The VPC subnet the instance(s) will go in"
}

variable "aws_ami" {
  description = "The AMI to use"
}

variable "number_of_instances" {
  description = "Number of instances to make"
  default     = 1
}

variable "tags" {
  description = "A mapping of tags to assign to the resource"
  default     = {}
}

variable "secgrp_id" {
  description = "The list of Security groups the instace will have"
  default     = []
}

variable "azs" {
  description = "A list of Availability zones in the region"
  default     = []
}

variable "ebs_optimized" {
  default = false
}
