//Outputs of AWS vpc module
output "public_subnets" {
  value = "${join(",", aws_subnet.public.*.id)}"
}

output "vpc_id" {
  value = "${aws_vpc.this.id}"
}

output "public_route_table_ids" {
  value = ["${aws_route_table.public.*.id}"]
}

output "default_security_group_id" {
  value = "${aws_vpc.this.default_security_group_id}"
}

output "igw_id" {
  value = "${aws_internet_gateway.this.id}"
}

output "public_subnets_list" {
  value = ["${aws_subnet.public.*.id}"]
}
