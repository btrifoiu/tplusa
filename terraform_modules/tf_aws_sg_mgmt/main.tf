provider "aws" {
  region = "${var.region}"
}

resource "aws_security_group" "mgmt" {
  name        = "mgmt"
  description = "Allow ssh access for management"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Free for all"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${merge(var.tags, map("Name", "mgmt-${var.secgrp_append_name}"))}"
}
