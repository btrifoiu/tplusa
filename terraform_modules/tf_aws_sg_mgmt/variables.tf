variable "cidr" {}

variable "vpc_id" {}

variable "secgrp_append_name" {}

variable "tags" {
  description = "A mapping of tags to assign to the resource"
  default     = {}
}

variable "region" {}
